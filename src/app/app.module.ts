import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { CurrencyMaskModule } from "ngx-currency-mask";
import {registerLocaleData} from '@angular/common';
import br from '@angular/common/locales/br';
import localeBrExtra from '@angular/common/locales/extra/br';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FinancialControlFormComponent } from './financial-control-form/financial-control-form.component';
import { FinancialControlStatementComponent } from './financial-control-statement/financial-control-statement.component';
import { HeaderComponent } from './header/header.component';

registerLocaleData(br, 'pt-BR', localeBrExtra);

@NgModule({
  declarations: [
    AppComponent,
    FinancialControlFormComponent,
    FinancialControlStatementComponent,
    HeaderComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CurrencyMaskModule
  ],
  providers: [ ],
  bootstrap: [AppComponent],

})
export class AppModule { }
