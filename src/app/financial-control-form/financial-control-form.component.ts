import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Tansaction } from '../transaction.model';
import { FinancialControlService } from '../financial-control.service';

@Component({
  selector: 'app-financial-control-form',
  templateUrl: './financial-control-form.component.html',
  styleUrls: ['./financial-control-form.component.scss']
})
export class FinancialControlFormComponent implements OnInit {

  transactionForm: FormGroup;
  @Output() insertItem = new EventEmitter<void>();

  constructor(private fb: FormBuilder, 
              private financialService: FinancialControlService
            ) {}

  ngOnInit() {
    this.transactionForm = this.fb.group({
      type: this.fb.control('',[Validators.required]),
      merchandise: this.fb.control('', [Validators.required]),
      value: this.fb.control('', [Validators.required])
    });
  }

  checkTrasaction(transaction: Tansaction) {
    this.financialService.addTransaction(transaction);
    this.insertItem.emit();
    this.transactionForm.reset();
  }
}