import { Injectable } from '@angular/core';

import { Tansaction } from './transaction.model';

@Injectable({
  providedIn: 'root'
})
export class FinancialControlService {

  transaction: Tansaction[] = [];
  constructor() { }

  addTransaction(item: Tansaction): void {
    
    if(localStorage.getItem("transaction") === null){
      this.transaction.push(item)
      localStorage.setItem("transaction",JSON.stringify(this.transaction))
    } else {
      this.transaction = JSON.parse(localStorage.getItem("transaction"));
      this.transaction.push(item);
      localStorage.setItem("transaction", JSON.stringify(this.transaction))
    }
  }

  getTransaction(): Tansaction[]{
    if(localStorage.getItem("transaction") === null) {
      return this.transaction;
    } else {
      this.transaction = JSON.parse(localStorage.getItem("transaction"))
      return this.transaction;
    }
  }

}
