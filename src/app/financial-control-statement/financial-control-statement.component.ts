import { Component, OnInit, Input } from '@angular/core';

import { FinancialControlService } from '../financial-control.service';
import { Tansaction } from '../transaction.model';

@Component({
  selector: 'app-financial-control-statement',
  templateUrl: './financial-control-statement.component.html',
  styleUrls: ['./financial-control-statement.component.scss']
})
export class FinancialControlStatementComponent implements OnInit {

  transaction: Tansaction[];

  constructor(private service: FinancialControlService) { }

  ngOnInit() {
    this.getInfo();
  }

  getInfo() {
    this.transaction =  this.service.getTransaction();
    this.total();
  }

  total():number {
    return this.transaction
      .map(item => 
        (item.type === "purchase")?Number(item.value)*(-1):Number(item.value)
      ).reduce((prev, value) => prev+value, 0)
    }
}
